#[macro_use]
extern crate nom;
extern crate regex;


use std::io::{stdin, BufRead};
use std::str::from_utf8_unchecked;

#[derive(Debug)]
struct Clause {
    literals: Vec<Predicate>
}

#[derive(Debug)]
struct Predicate {
    truth: bool,
    name: String,
    terms: Vec<Term>
}

#[derive(Debug)]
struct Function {
    name: String,
    terms: Vec<Term>
}

#[derive(Debug)]
enum Term {
    Constant(String),
    Variable(String),
    Function(Function)
}


fn is_lower(ch: u8) -> bool { ch >= b'a' && ch <= b'z' }
fn is_upper(ch: u8) -> bool { ch >= b'A' && ch <= b'Z' }
fn is_alpha(ch: u8) -> bool { is_lower(ch) || is_upper(ch) }
fn is_num(ch: u8) -> bool { ch >= b'0' && ch <= b'9' }
fn is_alphanum(ch: u8) -> bool { is_alpha(ch) || is_num(ch) }

named!(lowercase, take_while1!(is_lower));
named!(uppercase, take_while1!(is_upper));
named!(alphanumeric, take_while1!(is_alphanum));

/// Parses if the input starts with a lowercase
named!(variable, recognize!( chain!( lowercase ~ alphanumeric? , || () ) ) );

/// Parses if the input starts with an uppercase
named!(nonvariable, recognize!( chain!( uppercase ~ alphanumeric? , || () ) ) );

/// Parses a term list separated by a comma
named!(term_list< Vec<Term> >, separated_list!(tag!(","), term) );

/// Parses a predicate list separated by a pipe
named!(pred_list< Vec<Predicate> >, separated_list!(tag!("|"), predicate) );

/// Parses a predicate/function if it's a nonvariable that has a left paren
named!(function<Function>,
    chain!(
        name: nonvariable ~
        tag!("(") ~ args: term_list ~ tag!(")") ,
        || Function {
            name: unsafe { String::from(from_utf8_unchecked(name)) },
            terms: args
        }
    )
);

/// Parses a term into a type `Term`
named!(term<Term>,
    alt_complete!(
        variable => { | s | unsafe { Term::Variable(String::from(from_utf8_unchecked(s))) } } |
        function => { | f | Term::Function(f) } |
        nonvariable => { | s | unsafe { Term::Constant(String::from(from_utf8_unchecked(s))) } }
    )
);

/// Parses a predicate
named!(predicate<Predicate>,
    chain!(
        truth: tag!("-")? ~
        name: nonvariable ~
        tag!("(") ~ args: term_list ~ tag!(")") ,
        || Predicate {
            truth: truth.is_none(),
            name: unsafe { String::from(from_utf8_unchecked(name)) },
            terms: args
        }
    )
);

named!(clause<Clause>,
    alt_complete!(
        pred_list => { | p | Clause { literals: p } }
    )
);

fn main() {
    let stdin = stdin();
    let lock = stdin.lock();
    let mut lines = lock.lines();

    // First line
    for result in lines {
        let line = result.unwrap();

        // Remove whitespace
        let white_split = line.split_whitespace();
        let string = white_split.fold(String::new(), |mut acc, s| {
            acc.push_str(s);
            acc
        });

        println!("{:#?}", clause(string.as_bytes()));
    }
}
